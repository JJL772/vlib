/*

Object model stuff

*/
#pragma once

#include <string>

class ObjectBase
{
public:
	
	virtual std::string Serialize() const = 0;
	
	virtual void Deserialize(std::string buf) = 0;
};
