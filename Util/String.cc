/*

String utils

*/
#include "String.h"

inline bool v_quickcmp(char c, const char* toks, int ntok)
{
	for(int i = 0;i < ntok; i++)
	{
		if(c == toks[i])
			return true;
	}
	return false;
}

/*
str		- The string to tokenize, null terminated (or not)
tokens  - The tokens that split the string
ntok    - The number of tokens that split the string
nstr    - The number of new strings
term    - Terminator at end of string

returns an array of strings
*/
char** v_strtok(const char* str, const char* tokens, int ntok, int& nstr, char term = '\0')
{
	#ifndef _USE_SIMD

	//TODO: Make this better
	char* retbuf[256];
	int retbufind = 0, total_size = 0;

	for(long i = 0, start = -1; str[i] != term; i++)
	{
		if(!v_quickcmp(str[i], tokens, ntok) && start == -1)
		{
			start = i;
		}
		if(v_quickcmp(str[i], tokens, ntok) && start != -1)
		{
			long size = i-start;

			char* buf = (char*)malloc(sizeof(char) * size + 1);
			memcpy(buf, &str[start], size);

			buf[size] = '\0'; // Null term
			retbuf[retbufind] = buf;

			// Get ready for next iter
			retbufind++;
			start = -1;
		}
	}

	nstr = retbufind + 1;

	// Copy to return
	char** ret = (char**)malloc(sizeof(char*) * (retbufind+1));
	memcpy(ret, retbuf, retbufind+1);

	return ret;

	#else
	#error Not implemented.
	#endif //_USE_SIMD
}

/*
Advances the buffer until the specified token

buf		-	the buffer
pos		-	buffer index
size	-	buffer length total
toks	-	list of tokens to read until
ntok	-	number of tokens
*/
void v_buf_skip_until(const char* buf, size_t& pos, size_t size, const char* toks, int ntok)
{
	for(; pos < size; pos++)
	{
		if(v_quickcmp(buf[pos], toks, ntok))
			return;
	}
}

/*
Skips the specified tokens in the buffer

buf		-	the buffer
pos		-	current buffer index
size	-	size of the current buffer
toks	-	the tokens to skip
ntok	-	number of tokens
*/
void v_buf_skip(const char* buf, size_t& pos, size_t size, const char* toks, int ntok)
{
	for(; pos < size; pos++)
	{
		if(!v_quickcmp(buf[pos], toks, ntok))
			return;
	}
}

/*
Skips the specified tokens in the buffer

buf		- the buffer
pos		- position in buffer
size	- max size of buffer
tok		- token to skip
*/
void v_buf_skip(const char* buf, size_t& pos, size_t size, char tok)
{
	for(; pos < size; pos++)
	{
		if(buf[pos] != tok)
			return;
	}
}

/*
Advances the buffer until the specified token

buf		- the buffer
pos		- the pos in the buffer
size	- max size of the buffer
tok		- token to skip until
*/
void v_buf_skip_until(const char* buf, size_t& pos, size_t size, char tok)
{
	for(; pos < size; pos++)
	{
		if(buf[pos] == tok)
			return;
	}
}

/*
Reads from the buffer until the specified tokens are hit
Note: This is not NULL terminated

buf		- the buffer
pos		- the pos in buffer
size	- max size of buffer
toks	- the tokens to skip
ntok	- the number of tokens
len		- the final length of the string
*/
char* v_buf_read_until(const char* buf, size_t& pos, size_t size, const char* toks, int ntok, size_t& len)
{
	// Read in chunks of 256, so max of 256^2
	char* chunks[256];
	char str[256];
	int index = 0, cindex = 0;

	for(; pos < size; pos++, index++)
	{
		if(!v_quickcmp(buf[pos], toks, ntok))
		{
			str[index] = buf[pos];
			if(index >= 255)
			{
				chunks[cindex] = (char*)malloc(sizeof(char) * 256);
				memcpy(chunks[cindex], str, 256);
				index = 0;
				cindex++;
			}
			else
				index++;
		}
		else
			break;
	}

	// Assemble this all into a big last string
	size_t total_size = ((cindex+1)*256) + (index+1);
	char* ret = (char*)malloc(sizeof(char) * (total_size+1));

	for(size_t i = 0; i <= cindex; i++)
	{
		if(i != cindex)
			memcpy((ret+(i*256)), chunks[i], sizeof(char) * 256);
		else
			memcpy((ret+(i*256)), chunks[i], sizeof(char) * (index+1));
		// After copy, free
		free(chunks[i]);
	}
	ret[total_size-1] = '\0';
	len = total_size;

	return ret;
}

/*
Reads from the buffer until the specified token is hit, Note, this is not NULL terminated.

buf		- the buffer
pos		- the pos in buffer
size	- max size of buffer
tok		- the token to skip
len		- the final length of the string
*/
char* v_buf_read_until(const char* buf, size_t& pos, size_t size, char tok, size_t& len)
{
	// Read in chunks of 256, so max of 256^2
	char* chunks[256];
	char str[256];
	int index = 0, cindex = 0;

	for(; pos < size; pos++, index++)
	{
		if(buf[pos] != tok)
		{
			str[index] = buf[pos];
			if(index >= 255)
			{
				// If we're out of space on the stack allocated string, copy into the heap
				chunks[cindex] = (char*)malloc(sizeof(char) * 256);
				memcpy(chunks[cindex], str, 256);
				index = 0;
				cindex++;
			}
			else
				index++;
		}
		else
			break;
	}

	// Assemble this all into a big last string
	size_t total_size = ((cindex+1)*256) + (index+1);
	char* ret = (char*)malloc(sizeof(char) * (total_size+1));

	for(size_t i = 0; i <= cindex; i++)
	{
		if(i != cindex)
			memcpy((ret+(i*256)), chunks[i], sizeof(char) * 256);
		else
			memcpy((ret+(i*256)), chunks[i], sizeof(char) * (index+1));
		// After copy, free
		free(chunks[i]);
	}
	ret[total_size-1] = '\0';
	len = total_size;

	return ret;
}


/*
 R eads from the buffer until the specified token is hit                                           *
 this will not allocate a new string in memory, rather just a pointer to the string in memory
 
 buf		- the buffer
 pos		- pos in buffer
 size	- total buffer size
 tok		- the token to skip
 len		- the final length of the string
 */
const char* v_buf_read_in_place_until(const char* buf, size_t& pos, size_t size, char tok, size_t& len)
{
	for(; pos < size; pos++, len++)
	{
		if(buf[pos] == tok)
		{
			return &buf[pos];
		}
	}
	return NULL;
}

/*
 R eads from the buffer until the specified token is hit                                           *                          *
 this will not allocate a new string in memory, rather just a pointer to the string in memory
 This will also not return a null terminated string.
 
 buf		- the buffer
 pos		- pos in buffer
 size		- total buffer size
 tok		- the token to skip
 toklen		- number of tokens in the list
 len		- the final length of the string
 */
const char* v_buf_read_in_place_until(const char* buf, size_t& pos, size_t size, const char* tok, size_t toklen, size_t& len)
{
	for(; pos < size; pos++, len++)
	{
		if(v_quickcmp(buf[pos], tok, toklen))
		{
			return &buf[pos];
		}
	}
	return NULL;
}
