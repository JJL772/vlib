/*

Utils for strings and buffers

*/
#include <stddef.h>
#include <memory.h>
#include <string.h>
#include <stdlib.h>

inline bool v_quickcmp(char c, const char* toks, int ntok);

/*
str		- The string to tokenize, null terminated (or not)
tokens  - The tokens that split the string
ntok    - The number of tokens that split the string
nstr    - The number of new strings
term    - Terminator at end of string

returns an array of strings
*/
char** v_strtok(const char* str, const char* tokens, int ntok, int& nstr, char term = '\0');
/*
Advances the buffer until the specified token

buf		-	the buffer
pos		-	buffer index
size	-	buffer length total
toks	-	list of tokens to read until
ntok	-	number of tokens
*/
void v_buf_skip_until(const char* buf, size_t& pos, size_t size, const char* toks, int ntok);
/*
Skips the specified tokens in the buffer

buf		-	the buffer
pos		-	current buffer index
size	-	size of the current buffer
toks	-	the tokens to skip
ntok	-	number of tokens
*/
void v_buf_skip(const char* buf, size_t& pos, size_t size, const char* toks, int ntok);

/*
Skips the specified tokens in the buffer

buf		- the buffer
pos		- position in buffer
size	- max size of buffer
tok		- token to skip
*/
void v_buf_skip(const char* buf, size_t& pos, size_t size, char tok);

/*
Advances the buffer until the specified token

buf		- the buffer
pos		- the pos in the buffer
size	- max size of the buffer
tok		- token to skip until
*/
void v_buf_skip_until(const char* buf, size_t& pos, size_t size, char tok);

/*
Reads from the buffer until the specified tokens are hit

buf		- the buffer
pos		- the pos in buffer
size	- max size of buffer
toks	- the tokens to skip
ntok	- the number of tokens
len		- the final length of the string
*/
char* v_buf_read_until(const char* buf, size_t& pos, size_t size, const char* toks, int ntok, size_t& len);

/*
Reads from the buffer until the specified token is hit

buf		- the buffer
pos		- the pos in buffer
size	- max size of buffer
tok		- the token to skip
len		- the final length of the string
*/
char* v_buf_read_until(const char* buf, size_t& pos, size_t size, char tok, size_t& len);

/*
Reads from the buffer until the specified token is hit
this will not allocate a new string in memory, rather just a pointer to the string in memory
This will also not return a null terminated string.

buf		- the buffer
pos		- pos in buffer
size	- total buffer size
tok		- the token to skip
len		- the final length of the string
*/
const char* v_buf_read_in_place_until(const char* buf, size_t& pos, size_t size, char tok, size_t& len);

/*
 Reads from the buffer until the specified token is hit                                           *
 this will not allocate a new string in memory, rather just a pointer to the string in memory
 This will also not return a null terminated string.
 
 buf		- the buffer
 pos		- pos in buffer
 size		- total buffer size
 tok		- the token to skip
 toklen		- number of tokens in the list
 len		- the final length of the string
 */
const char* v_buf_read_in_place_until(const char* buf, size_t& pos, size_t size, const char* tok, size_t toklen, size_t& len);
