/*

JSONParse.h

JSON parsing stuff

*/
#pragma once

#include <type_traits>
#include <string>
#include <exception>
#include <vector>
#include <ios>
#include <fstream>
#include <optional>

#include "ObjectModel.h"

enum class EJSONPropertyType
{
	ANY,
	STRING,
	INTEGER,
	FLOAT,
	ARRAY,
	NESTED,
	CUSTOM,
};

class JSONPropertyBase
{
private:
public:
	
	virtual EJSONPropertyType GetType() const { return EJSONPropertyType::ANY; }
};

template<class T>
class JSONProperty : public JSONPropertyBase
{
private:
	EJSONPropertyType m_type;
	std::string m_name;
public:
	JSONProperty(std::string m_name) 
	{
		// Determine types
		if(std::is_integral<T>::value)
			m_type = EJSONPropertyType::INTEGER;
		else if(std::is_floating_point<T>::value)
			m_type = EJSONPropertyType::FLOAT;
		else if(std::is_same<T, std::string>::value)
			m_type = EJSONPropertyType::STRING;
		else if(std::is_class<T>::value)
		{
			if(std::is_base_of<ObjectBase>::value)
				m_type = EJSONPropertyType::CUSTOM;
			else
				throw new std::exception();
		}
		
		this->m_name = m_name;
	}
	
	virtual EJSONPropertyType GetType() const
	{
		return m_type;
	}
};

class JSONParser
{
public:
	JSONParser(std::istream buf);
	
	std::vector<JSONPropertyBase*> Parse();
};


class JSONDocument
{
protected:
	std::vector<JSONPropertyBase*> m_Properties;
	
public:
	template<class T>
	void SetProperty(std::string name, T value);
	
	template<class T>
	std::optional<T> GetProperty(std::string name) const;
	
	void Parse(std::string file);
	
	void Save(std::string file);
	
	void ParseBuffer(std::istream stream);
	
	void SaveBuffer(std::ostream stream);
};


#define BEGIN_JSON_OBJECT(name)\
class name : public JSONDocument	\
{	\
	name()		\
	{\
		m_Properties = std::vector<JSONPropertyBase*>();

#define DEFINE_JSON_PROPERTY(type, name)\
		m_Properties.push_back(new JSONProperty<T>(#name));
			
#define END_JSON_OBJECT(name)\
	}\
};
